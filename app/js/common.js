$(function() {

	// Custom JS
    pulse();

    $('.btn_video, .about__video--thumb').on('click', function (object) {
       var target = '#'+$(this).data('target');
        $('.about__video--thumb').fadeOut();
        if ($(target).get(0).paused) {
            $(target).get(0).play();
        } else {
            $(target).get(0).pause();
        }
        $(target).get(0).play()
    });


    $('.about__author-blocks').on('click', function (e) {
        $('.about__author-blocks').removeClass('active');
        $(this).addClass('active');
        var target = '.'+$(this).data('target');
        $('.about__author-desc').removeClass('active');
        $(target).addClass('active');

        return false;

    })


    /** Circle donut chart**/
    $('.circle').circleProgress({
        size: 150,
        thickness: 3,
        emptyFill: 'rgba(180,180,180,.3)',
        fill:  'rgba(100,50,150,.8)',
    }).on('circle-animation-progress', function(event, progress) {
        var num = $(this).data('num');
        $(this).find('strong').html(Math.round(num * progress) + '<i></i>');
        pureNum()
    });



    /** Num mask 99 999 **/
    function pureNum(){
        var prices = document.querySelectorAll(".num_mask");
        Array.from(prices).forEach(function(elem){
            elem.innerHTML = elem.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        });
    }


    /** Btn pulse animation **/
    function pulse() {
        $('.pulse').each(function () {
            var $this = $(this);
            var ink, d, x, y;

            setInterval(function () {
                if ($this.find(".ink").length === 0) {
                    $this.prepend("<span class='ink'></span>");
                }

                ink = $this.find(".ink");
                ink.removeClass("animate");

                if (!ink.height() && !ink.width()) {
                    d = Math.max($this.outerWidth(), $this.outerHeight());
                    ink.css({ height: d, width: d });
                }

                x = Math.round(Math.random() * ink.width() - ink.width() / 2);
                y = Math.round(Math.random() * ink.height() - ink.height() / 2);
                // y = 0;
                // x = e.pageX - $this.offset().left - ink.width()/2;
                // y = e.pageY - $this.offset().top - ink.height()/2;

                ink.css({ top: y + 'px', left: x + 'px' }).addClass("animate");
            }, 1600);
        });
    };


});
